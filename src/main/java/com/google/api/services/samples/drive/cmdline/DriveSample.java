/*
 * Copyright (c) 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.google.api.services.samples.drive.cmdline;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.Preconditions;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttachment;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;

import org.apache.commons.lang3.StringUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * A sample application that runs multiple requests against the Drive API. The requests this sample
 * makes are:
 * <ul>
 * <li>Does a resumable media upload</li>
 * <li>Updates the uploaded file by renaming it</li>
 * <li>Does a resumable media download</li>
 * <li>Does a direct media upload</li>
 * <li>Does a direct media download</li>
 * </ul>
 *
 * @author rmistry@google.com (Ravi Mistry)
 */
public class DriveSample {

  /**
   * 
   */
  private static final String _ROOT = "root";

  /**
   * Be sure to specify the name of your application. If the application name is {@code null} or
   * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
   */
  private static final String APPLICATION_NAME = "";

  private static final String UPLOAD_FILE_PATH = "sample.png";
  private static final String DIR_FOR_DOWNLOADS = ".";
  private static final java.io.File UPLOAD_FILE = new java.io.File(UPLOAD_FILE_PATH);

  /** Directory to store user credentials. */
  private static final java.io.File DATA_STORE_DIR =
      new java.io.File(System.getProperty("user.home"), ".store/drive_sample");

  /**
   * Global instance of the {@link DataStoreFactory}. The best practice is to make it a single
   * globally shared instance across your application.
   */
  private static FileDataStoreFactory dataStoreFactory;

  /** Global instance of the HTTP transport. */
  private static HttpTransport httpTransport;

  /** Global instance of the JSON factory. */
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

  /** Global Drive API client. */
  private static Drive drive;

  private static com.google.api.services.calendar.Calendar calendarService;


  /** Authorizes the installed application to access user's protected data. */
  private static Credential authorize() throws Exception {
    // load client secrets
    GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY,
        new InputStreamReader(DriveSample.class.getResourceAsStream("/client_secrets.json")));
    if (clientSecrets.getDetails().getClientId().startsWith("Enter")
        || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
      System.out.println(
          "Enter Client ID and Secret from https://code.google.com/apis/console/?api=drive "
              + "into drive-cmdline-sample/src/main/resources/client_secrets.json");
      System.exit(1);
    }
    // set up authorization code flow
    GoogleAuthorizationCodeFlow flow =
        new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
            Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(dataStoreFactory).build();
    // authorize
    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
  }

  private static Credential serviceAccountAuthorize() throws Exception {

    GoogleCredential credential = GoogleCredential
        .fromStream(DriveSample.class.getResourceAsStream("/ovs-poc-826f1a05c002.json"))
        .createScoped(Arrays.asList(DriveScopes.DRIVE, CalendarScopes.CALENDAR));

    return credential;
  }

  public static void main(String[] args) {
    Preconditions.checkArgument(
        !UPLOAD_FILE_PATH.startsWith("Enter ") && !DIR_FOR_DOWNLOADS.startsWith("Enter "),
        "Please enter the upload file path and download directory in %s", DriveSample.class);

    try {
      httpTransport = GoogleNetHttpTransport.newTrustedTransport();
      dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);
      // authorization

      // Credential credential = authorize();

      Credential credential = serviceAccountAuthorize();

      // set up the global Drive instance
      drive = new Drive.Builder(httpTransport, JSON_FACTORY, credential)
          .setApplicationName(APPLICATION_NAME).build();

      calendarService = new com.google.api.services.calendar.Calendar.Builder(httpTransport,
          JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();

      // run commands

      printAbout(drive);

      View.header1("DRIVE");

      View.header1("create path");

      HashMap<String, String> tags = new HashMap<String, String>() {
        {
          put("code", "56345 34563");
          put("lang", "it");
        }
      };

      File folder = prepareFolder(new String[] {"test1", "test13"}, "test132", tags);

      // View.header1("SHARE ");

      // shareFile(folder.getId(), "daniele.baggio.srv@gmail.com");

      View.header1("Find by tags");

      HashMap<String, String> tags2 = new HashMap<String, String>() {
        {
          put("lang", "it");
        }
      };

      List<File> folderFound = findFolderByTags(tags2);

      // View.header1("List shared files");
      // retrieveAllFolders(drive);

      View.header1("Starting Resumable Media Upload");

      File uploadedFile = uploadFile(folderFound, UPLOAD_FILE, tags2);

      String[] emails = new String[] {"daniele.baggio.srv@gmail.com"};

      shareFile(uploadedFile, emails);

      View.header1("Starting Simple Media Download");

      downloadFile(true, uploadedFile);

      // View.header1("CALENDAR");

      // show10Events();

      // addEventAndInvite(uploadedFile, "alberto.deste@sysdata.it");

      return;

    } catch (IOException e) {
      System.err.println(e.getMessage());
    } catch (Throwable t) {
      t.printStackTrace();
    }
    System.exit(1);
  }


  private static void printAbout(Drive service) {
    try {
      About about = service.about().get().setFields("user, storageQuota").execute();

      System.out.println("Total quota (bytes): " + about.getStorageQuota().getLimit());
    } catch (IOException e) {
      System.out.println("An error occurred: " + e);
    }
  }

  private static File prepareFolder(String[] parentNames, String name, Map<String, String> tags)
      throws Exception {

    if (parentNames == null || parentNames.length == 0) {

      return prepareFolder(_ROOT, name, tags);
    }

    if (parentNames.length > 0) {

      File basefolder = prepareFolder(_ROOT, parentNames[0], null);

      for (int i = 1; i < parentNames.length - 1; i++) {

        basefolder = prepareFolder(basefolder.getId(), parentNames[i], null);
      }

      return prepareFolder(basefolder.getId(), name, tags);
    }

    throw new Exception("invalid path");
  }

  private static File prepareFolder(final String parentId, final String name,
      final Map<String, String> tags) throws Exception {

    String q = String.format("mimeType='application/vnd.google-apps.folder' and trashed=false "
        + "and '%s' in parents " + "and name = '%s' ", parentId, name);

    Files.List request = drive.files().list().setQ(q);

    FileList files = request.execute();

    if (files.getFiles().size() == 0) {

      File fileMetadata = new File();

      fileMetadata.setName(name);

      fileMetadata.setParents(new ArrayList<String>() {
        {
          add(parentId);
        }
      });

      fileMetadata.setMimeType("application/vnd.google-apps.folder");

      _assignProperties(tags, fileMetadata);

      File folder = drive.files().create(fileMetadata).setFields("id").execute();

      System.out.println("created " + folder.toPrettyString());

      return folder;
    }

    if (files.getFiles().size() > 1) {

      System.out.println("WARNING, multiple found for " + q);
    }

    if (files.getFiles().size() > 0) {

      File folder = files.getFiles().get(0);

      if (tags != null) {

        File newContent = new File();

        _assignProperties(tags, newContent);

        folder = drive.files().update(folder.getId(), newContent).execute();

        System.out.println("updated " + folder.toPrettyString());
      }

      return folder;
    }

    throw new Exception("invalid path");
  }

  private static List<File> findFolderByTags(HashMap<String, String> tags) throws IOException {

    String q = String.format("mimeType='application/vnd.google-apps.folder' and trashed=false");

    if (tags.keySet().size() > 0) {

      String[] sb = new String[tags.keySet().size()];
      int i = 0;

      for (final String key : tags.keySet()) {

        sb[i++] = String.format("properties has {key='%s' and value='%s'}", key, tags.get(key));
      }

      q = q + " and " + StringUtils.join(sb, " and ");
    }

    System.out.println(q);

    Files.List request = drive.files().list().setQ(q);

    List<File> result = new ArrayList<File>();

    do {

      try {
        FileList files = request.execute();

        result.addAll(files.getFiles());

        request.setPageToken(files.getNextPageToken());

      } catch (IOException e) {
        System.out.println("An error occurred: " + e);
        request.setPageToken(null);
      }

    } while (request.getPageToken() != null && request.getPageToken().length() > 0);

    for (File f : result) {

      System.out.println(f.toPrettyString());
    }

    return result;
  }

  /**
   * @param tags
   * @param fileMetadata
   */
  private static void _assignProperties(final Map<String, String> tags, File fileMetadata) {

    if (tags != null) {

      fileMetadata.setProperties(tags);

      if (tags.keySet().size() > 0) {

        String[] sb = new String[tags.keySet().size()];
        int i = 0;

        for (final String key : tags.keySet()) {

          sb[i++] = String.format("%s=%s", key, tags.get(key));
        }

        fileMetadata.setDescription(StringUtils.join(sb, ", "));
      }
    }
  }

  private static List<File> retrieveAllFolders(Drive service) throws IOException {
    List<File> result = new ArrayList<File>();

    Files.List request =
        service.files().list().setQ("mimeType='application/vnd.google-apps.folder'")
            .setFields("items(id,name,parents,properties)");

    do {

      try {
        FileList files = request.execute();

        result.addAll(files.getFiles());

        request.setPageToken(files.getNextPageToken());

      } catch (IOException e) {
        System.out.println("An error occurred: " + e);
        request.setPageToken(null);
      }

    } while (request.getPageToken() != null && request.getPageToken().length() > 0);

    for (File f : result) {

      System.out.println(f.toPrettyString());
    }

    return result;
  }

  /** Uploads a file using either resumable or direct media upload. */
  private static File uploadFile(final List<File> inAllFolders, java.io.File file,
      Map<String, String> tags) throws IOException {

    File fileMetadata = new File();

    fileMetadata.setName(UPLOAD_FILE.getName());

    List<String> parents = new ArrayList<String>() {
      {
        for (File folder : inAllFolders) {
          add(folder.getId());
        }
      }
    };

    fileMetadata.setParents(parents);

    _assignProperties(tags, fileMetadata);

    FileContent mediaContent =
        new FileContent(java.nio.file.Files.probeContentType(file.toPath()), file);

    Drive.Files.Create insert = drive.files().create(fileMetadata, mediaContent);

    MediaHttpUploader uploader = insert.getMediaHttpUploader();

    uploader.setDirectUploadEnabled(true);

    uploader.setProgressListener(new FileUploadProgressListener());

    File ff = insert.execute();

    return ff;
  }

  /** Updates the name of the uploaded file to have a "drivetest-" prefix. */
  private static File updateFileWithTestSuffix(String id) throws IOException {
    File fileMetadata = new File();
    fileMetadata.setName("drivetest-" + UPLOAD_FILE.getName());

    Drive.Files.Update update = drive.files().update(id, fileMetadata);
    return update.execute();
  }

  private static void shareFile(File file, String[] emails) throws IOException {

    Permission userPermission = new Permission().setType("user").setRole("reader");

    for (String email : emails) {

      userPermission.setEmailAddress(email);
    }

    drive.permissions().create(file.getId(), userPermission).execute();
  }

  /** Downloads a file using either resumable or direct media download. */
  private static void downloadFile(boolean useDirectDownload, File uploadedFile)
      throws IOException {
    // create parent directory (if necessary)
    java.io.File parentDir = new java.io.File(DIR_FOR_DOWNLOADS);

    if (!parentDir.exists() && !parentDir.mkdirs()) {
      throw new IOException("Unable to create parent directory");
    }

    OutputStream out = new FileOutputStream(new java.io.File(parentDir, uploadedFile.getName()));

    drive.files().get(uploadedFile.getId()).executeMediaAndDownloadTo(out);
  }

  private static void showCalendars() throws IOException {
    View.header1("Show Calendars");

    CalendarList feed = calendarService.calendarList().list().execute();

    View.display(feed);
  }

  private static void show10Events() throws IOException {

    DateTime now = new DateTime(System.currentTimeMillis());

    Events events = calendarService.events().list("primary").setMaxResults(10).setTimeMin(now)
        .setOrderBy("startTime").setSingleEvents(true).execute();

    List<Event> items = events.getItems();

    if (items.size() == 0) {

      System.out.println("No upcoming events found.");

    } else {

      System.out.println("Upcoming events");

      for (Event event : items) {

        DateTime start = event.getStart().getDateTime();
        if (start == null) {
          start = event.getStart().getDate();
        }

        System.out.printf("%s (%s)\n", event.getSummary(), start);
      }
    }
  }

  private static Calendar addCalendarPrimary() throws IOException {

    Calendar entry = new Calendar();

    entry.setSummary("Primary cal");

    Calendar result = calendarService.calendars().insert(entry).execute();

    View.display(result);

    return result;
  }

  private static void addEventAndInvite(File driveFile, String email) throws IOException {

    View.header1("Add Event");

    Event event = new Event();
    event.setSummary("New Event " + (new Date()).toString());

    Date startDate = new Date();
    Date endDate = new Date(startDate.getTime() + 4 * 3600000);
    DateTime start = new DateTime(startDate, TimeZone.getTimeZone("UTC"));
    event.setStart(new EventDateTime().setDateTime(start));
    DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
    event.setEnd(new EventDateTime().setDateTime(end));


    if (email != null) {

      event.setAttendees(
          Collections.singletonList(new EventAttendee().setEmail(email).setOptional(true)));

      System.out.println("invite " + email);
    }

    if (driveFile != null) {

      event.setAttachments(
          Collections.singletonList(new EventAttachment().setFileId(driveFile.getId())));

      System.out.println("attach " + driveFile);
    }

    Event result =
        calendarService.events().insert("primary", event).setSendNotifications(true).execute();

    View.display(result);
  }


}
